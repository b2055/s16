let inputNum = Number(prompt("Please input a number: "));

console.log("The number you provided is: " + inputNum);

for (; inputNum >= 0; inputNum--) {
	if (inputNum <= 50) {
		console.log("Terminating Loop value is at 50 or less")
		break;
	}
	else if (inputNum % 10 === 0) {
		console.log("Number is divisible by 10 skipping");
		continue;
	} 
	else if (inputNum % 5 === 0) {
		console.log(inputNum);
	}
}


let stringSample = "supercalifragilisticexpialidocious";
let stringStore = "";

console.log(stringSample)

for (let x = 0; x < stringSample.length; x++) {
	if (stringSample[x].toLowerCase() == 'a' || 
		stringSample[x].toLowerCase() == 'e' || 
		stringSample[x].toLowerCase() == 'i' || 
		stringSample[x].toLowerCase() == 'o' || 
		stringSample[x].toLowerCase() == 'u') {
		continue;
	}
	else {
		stringStore += stringSample[x];
	}
}

console.log(stringStore);
